Below, please find sample visualizations of the graphs in this directory.
These visualizations do not regard any vertical or horizontal padding.


graph-backedge.json
=================
_____________
⇓           |
1           |
|________   |
⇓   ⇓   ⇓   |
2   3   4   |
________|   |
⇓           |
5           |
|___________|

graph-extensive.json
====================

1
|________________
⇓   ⇓   ⇓   ⇓   ⇓
2   3   4   5   6
|   |           |
|___|___________|
⇓   ⇓
7   8
|   |
|___|
⇓   ⇓
9   10