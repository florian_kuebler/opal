/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package tac

import java.util.Arrays
import org.scalatest.FunSpec
import org.scalatest.Matchers

/**
 * Common superclass of all TAC unit tests.
 *
 * @author Michael Eichberg
 */
private[tac] class TACTest extends FunSpec with Matchers {

    def compareStatements(expectedStmts: IndexedSeq[Stmt], actualStmts: IndexedSeq[Stmt]): Unit = {
        compareStatements(expectedStmts.toArray, actualStmts.toArray)
    }

    def compareStatements(expectedStmts: Array[Stmt], actualStmts: Array[Stmt]): Unit = {
        val expected = expectedStmts.asInstanceOf[Array[Object]]
        val actual = actualStmts.asInstanceOf[Array[Object]]
        if (!Arrays.equals(expected, actual)) {
            val message =
                actualStmts.zip(expectedStmts).
                    filter(p ⇒ p._1 != p._2).
                    map(p ⇒ "\t"+p._1+"\n\t<=>[Expected:]\n\t"+p._2+"\n").
                    mkString("Differences:\n", "\n", "\n")
            fail(message)
        }
    }
}
